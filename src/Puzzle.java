import java.util.*;

/**
 * Word puzzle.
 *
 * @since 1.8
 */
public class Puzzle {

    /**
     * Solve the word puzzle.
     *
     * @param args three words (addend1 addend2 sum)
     */
    public static void main(String[] args) {
        if (args != null || args.length != 3) {
            List<Map<String, Integer>> solutions = solver(args);
            System.out.println(args[0] + " + " + args[1] + " = " + args[2]);
            if(solutions.size() > 0){
                System.out.println("Number of possible solutions: " + solutions.size());
                System.out.println(solver(args));

            }else{
                System.out.println("No solutions found.");
            }
        }
    }

    private static List<Map<String, Integer>> solver(String[] list) {
        List<Map<String, Integer>> solutions = new ArrayList<>();
        char[] uniqueChars = getCharArray(list);
        int[] cantBeZero = cantBeZero(uniqueChars, list);
        int[] charvalues = new int[uniqueChars.length];
        int n = 0;

        while (n >= 0) {
            if (charvalues[n] < 10) {
                charvalues[n]++;
                if (isValuePlausible(n, charvalues, cantBeZero)) {
                    if (n < uniqueChars.length - 1) {
                        n++;
                    } else {
                        if (doValuesWork(charvalues, uniqueChars, list)) {
                            solutions.add(resultMap(charvalues, uniqueChars));
                        }
                    }
                }
            } else {
                charvalues[n] = 0;
                n--;
            }
        }
        return solutions;
    }


    /**
     * Changed letters into numbers to see if generated operation is valid.
     *
     * @param values - randomly generated int array, that contains values from 1 to 10
     * @param charArray - array of all unique chars that program was given as input.
     * @param args - arguments program was given when program was started.
     * @return true, if letters changed to generated values makes valid sum operation
     */
    private static boolean doValuesWork(int[] values, char[] charArray, String[] args) {
        String[] numberHolder = new String[3];

        for (int i = 0; i < 3; i++) {
            StringBuilder longString = new StringBuilder();
            for (int j = 0; j < args[i].length(); j++) {
                int index = new String(charArray).indexOf(args[i].charAt(j));
                longString.append(values[index] - 1);
            }
            numberHolder[i] = longString.toString();
        }

        return Long.parseLong(numberHolder[0]) + Long.parseLong(numberHolder[1]) == Long.parseLong(numberHolder[2]);
    }

    /**
     * Used to get operation first chars, what cannot be equal to zero.
     *
     * @param uniqueChars - array of all unique chars that program was given as input.
     * @param list - arguments program was given when program was started.
     * @return locations of chars in uniqueChar array, that cannot be equal to 0.
     */
    //https://stackoverflow.com/questions/50695659/getting-index-of-a-char-from-a-char-array
    private static int[] cantBeZero(char[] uniqueChars, String[] list) {
        int[] result = new int[list.length];

        for (int i = 0; i < 3; i++) {
            char charWeAreLookingFor = list[i].charAt(0);
            int found = new String(uniqueChars).indexOf(charWeAreLookingFor);
            result[i] = found;
        }
        return result;
    }


    /**
     * Checks if value is in right range and isn't in the list twice.
     *
     * @param k - location of current value in charvalue array
     * @param charvaluesArray -  all values that have been assigned.
     * @param cantBeZero -
     * @return true if array every member is different
     */
    private static boolean isValuePlausible(int k, int[] charvaluesArray, int[] cantBeZero) {
        if (charvaluesArray[k] == 0) {
            return true;
        }

        if (k > 9) {
            throw new RuntimeException("index problem");
        }

        if (k < 1) {
            return true;
        }

        //check if value already exists.
        for (int i = 0; i < k; i++) {
            if (charvaluesArray[i] == charvaluesArray[k]) {
                return false;
            }
        }
        for (int number : cantBeZero){
            if(charvaluesArray[number] == 1){
                return false;
            }
        }

        return true;
    }


    private static Map<String, Integer> resultMap(int[] values, char[] character) {

        HashMap<String, Integer> result = new HashMap<String, Integer>();

        for (int j = 0; j < character.length; j++) {
            result.put(String.valueOf(character[j]), values[j] - 1);
        }
        return result;
    }

    /**
     * Gets all unique characters that are inputs have.
     *
     * @param stringArray list of Strings
     * @return Set with unique chars that were in that list.
     */
    private static char[] getCharArray(String[] stringArray) {
        char[] result = new char[0];
        int currentLength = 0;

        for (String word : stringArray) {
            for (char c : word.toCharArray()) {
                if (!arrayContainsChar(result, c)) {
                    currentLength++;
                    char[] newArray = new char[currentLength];
                    System.arraycopy(result, 0, newArray, 0, result.length);
                    result = newArray;
                    result[currentLength - 1] = c;

                }
            }
        }
        return result;
    }


    /**
     * Check if char array contains certain char
     *
     * @param array     char array where you want to check existance of certain char
     * @param character a character what you want you are looking for
     * @return does array contain certain character.
     */
    private static boolean arrayContainsChar(char[] array, char character) {
        for (char ch : array) {
            if (character == ch) {
                return true;
            }
        }
        return false;
    }
}

